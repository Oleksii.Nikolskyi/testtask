﻿using System;
using System.Collections;
using System.Linq;

namespace TestTask
{
    public class TestTask
    {
        //  Есть последовательность целых положительных чисел от значения min до значения max(включительно).
        //  При этом каждое последующее число в последовательности на единицу больше предыдущего.
        //  Например, если значение min = 10, a max = 15, то числа последовательности будут представлены следующим образом.
        //  10, 11, 12, 13, 14, 15
        //  Для каждого из чисел последовательности необходимо вычислить сумму цифр, из которых это число состоит,
        //  найти те из них сумма цифр которых будет равна значению sum.
        //  Например, целое число 125 состоит из трех цифр – 1, 2, 5, если мы посчитаем сумму этих цифр,
        //  то получим 1 + 2 + 5 = 8.
        //  И если sum = 8, то число 125 будет удовлетворять критериям поиска, поскольку сумма цифр 125 совпадает с sum.
        //  По условиям задачи необходимо найти все числа в последовательности сумма цифр которых совпадает с sum.
        //  min, max и sum являются параметрами и могут принимать любые значения.

        public static IEnumerable Task(int min, int max, int sum)
        {
            if (min > max) throw new ArgumentException($"Was greater than {nameof(max)}", nameof(min));
            if (min < 0 || max < 0)
            {
                string param = min < 0 ? nameof(min) : nameof(max);
                throw new ArgumentOutOfRangeException(param, $"Was less than 0");
            }
            return Enumerable
                .Range(min, max - min + 1)
                .Where(x =>
                {
                    int s = 0;
                    while (x != 0)
                    {
                        s += x % 10;
                        x /= 10;
                    }
                    return s == sum;
                });
        }
    }
}
