using NUnit.Framework;
using System;
using System.Linq;

namespace TestTask.Tests
{
    public class Tests
    {
        [TestCase(0, 10, 1, new int[] { 1, 10 })]
        [TestCase(0, 10, 5, new int[] { 5 })]
        [TestCase(0, 126, 8, new int[] { 8, 17, 26, 35, 44, 53, 62, 71, 80, 107, 116, 125 })]
        [TestCase(1, 100, -1, new int[] { })]
        [TestCase(1, 1, 2, new int[] { })]
        [TestCase(1, 1, 1, new int[] { 1 })]

        public void Task_ReturnsCorrectValue(int min, int max, int sum, int[] excpected)
        {
            // Arrange

            // Act
            var actual = TestTask.Task(min, max, sum);

            // Assert
            Assert.AreEqual(excpected.ToList(), actual);
        }

        [TestCase(4, 1, 2)]
        public void Task_MaxLessMin_ThrowArgumentException(int min, int max, int sum)
        {
            // arrange
            var expectedTypeError = typeof(ArgumentException);
            // act 
            Exception ex = Assert.Catch(() => TestTask.Task(min, max, sum));
            // assert
            Assert.AreEqual(expectedTypeError, ex.GetType());
        }

        [TestCase(-1, 3, 2)]
        [TestCase(-3, -1, 2)]
        public void Task_MinOrMaxLessZero_ThrowArgumentOutOfRangeException(int min, int max, int sum)
        {
            // arrange
            var expectedTypeError = typeof(ArgumentOutOfRangeException);
            // act 
            Exception ex = Assert.Catch(() => TestTask.Task(min, max, sum));
            // assert
            Assert.AreEqual(expectedTypeError, ex.GetType());
        }
    }
}